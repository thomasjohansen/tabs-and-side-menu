import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Tabs and side menu',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
